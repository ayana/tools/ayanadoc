'use strict';

const path = require('path');
const fs = require('fs');

const { DefaultTheme } = require('typedoc/dist/lib/output/themes/DefaultTheme');
const { ProjectReflection } = require('typedoc/dist/lib/models/reflections/project');
const { UrlMapping } = require('typedoc/dist/lib/output/models/UrlMapping');
const { ReflectionKind } = require('typedoc/dist/lib/models/reflections/abstract');
const { DeclarationReflection } = require('typedoc/dist/lib/models/reflections');
const { NavigationItem } = require('typedoc/dist/lib/output/models/NavigationItem');

const moduleRegex = /^(@(?:[a-z0-9_-]+)\/)(?:[a-z0-9_-]+)$/;

DefaultTheme.getUrl = function (reflection, relative) {
	const separator = '/';

	let url = reflection.name;

	if (url.indexOf('@') > -1) {
		const matches = moduleRegex.exec(reflection.name);

		if (matches) {
			url = reflection.name.substring(matches[1].length);

			if (reflection.kindOf(ReflectionKind.SomeModule)) {
				reflection.moduleUrlName = url;
			}
		}
	}

	if (reflection.parent && reflection.parent !== relative &&
		!(reflection.parent instanceof ProjectReflection)) {
		url = DefaultTheme.getUrl(reflection.parent, relative, separator) + separator + url;
	}

	return encodeURI(url);
};

DefaultTheme.buildUrls = function (reflection, urls) {
	const mapping = DefaultTheme.getMapping(reflection);
	if (mapping) {
		if (!reflection.url || !DefaultTheme.URL_PREFIX.test(reflection.url)) {
			const url = [mapping.directory, DefaultTheme.getUrl(reflection) + '.html'].join('/');
			urls.push(new UrlMapping(url, reflection, mapping.template));
			reflection.url = url;
			reflection.hasOwnDocument = true;
		}

		// eslint-disable-next-line guard-for-in
		for (const key in reflection.children) {
			const child = reflection.children[key];
			if (mapping.isLeaf) {
				DefaultTheme.applyAnchorUrl(child, reflection);
			} else {
				DefaultTheme.buildUrls(child, urls);
			}
		}
	} else {
		DefaultTheme.applyAnchorUrl(reflection, reflection.parent);
	}

	return urls;
};

DefaultTheme.prototype.getUrls = function (project) {
	const urls = [];
	const entryPoint = this.getEntryPoint(project);

	entryPoint.url = 'index.html';
	entryPoint.introduction = fs.readFileSync(path.join(__dirname, '../INTRODUCTION.md'), { encoding: 'utf8' });
	urls.push(new UrlMapping('index.html', entryPoint, 'reflection.hbs'));

	if (entryPoint.children) {
		entryPoint.children.forEach(function (child) {
			if (child instanceof DeclarationReflection) {
				DefaultTheme.buildUrls(child, urls);
			}
		});
	}

	const reflections = entryPoint.getChildrenByKind(ReflectionKind.SomeModule);
	reflections.forEach(function (reflection) {
		const documents = reflection.documents;
		if (documents) {
			const docBaseUrl = `documents/${reflection.moduleUrlName}`;
			for (const document of documents) {
				const docUrl = `${docBaseUrl}/${document.urlName || document.name.toLowerCase()}.html`;

				document.readme = document.content;
				document.url = docUrl;
				document.document = true;
				document.parent = document.parent || reflection;
				document.sortIndex = typeof document.sortIndex === 'number' ? document.sortIndex : Number.MAX_SAFE_INTEGER;

				const mapping = new UrlMapping(docUrl, document, 'index.hbs');
				mapping.document = true;
				urls.push(mapping);

				document.url = docUrl;
			}
		}
	});

	return urls;
};

DefaultTheme.prototype.getNavigation = function (project) {
	const entryPoint = this.getEntryPoint(project);

	function containsExternals(modules) {
		for (let index = 0, len = modules.length; index < len; index++) {
			if (modules[index].flags.isExternal) {
				return true;
			}
		}
		return false;
	}
	function sortReflections(modules) {
		modules.sort(function (a, b) {
			if (a.flags.isExternal && !b.flags.isExternal) {
				return 1;
			}
			if (!a.flags.isExternal && b.flags.isExternal) {
				return -1;
			}
			return a.getFullName() < b.getFullName() ? -1 : 1;
		});
	}
	function includeDedicatedUrls(reflection, item) {
		(function walk(reflection) {
			// eslint-disable-next-line guard-for-in
			for (const key in reflection.children) {
				const child = reflection.children[key];
				if (child.hasOwnDocument && !child.kindOf(ReflectionKind.SomeModule)) {
					if (!item.dedicatedUrls) {
						item.dedicatedUrls = [];
					}
					item.dedicatedUrls.push(child.url);
					walk(child);
				}
			}
		})(reflection);
	}
	function buildChildren(reflection, parent) {
		const modules = reflection.getChildrenByKind(ReflectionKind.SomeModule);
		modules.sort(function (a, b) {
			return a.getFullName() < b.getFullName() ? -1 : 1;
		});
		modules.forEach(function (reflection) {
			const item = NavigationItem.create(reflection, parent);
			includeDedicatedUrls(reflection, item);
			buildChildren(reflection, item);
		});
	}
	function buildDocuments(item, beforeSubmodules) {
		if (item.reflection.documents) {
			item.reflection.documents.sort((a, b) => {
				if (a.sortIndex !== b.sortIndex) {
					return a.sortIndex < b.sortIndex ? -1 : 1;
				}
				if (a.name !== b.name) {
					return a.name < b.name ? -1 : 1;
				}

				return 0;
			});

			for (const document of item.reflection.documents) {
				if (Boolean(document.beforeSubmodules) !== beforeSubmodules) continue;
				// eslint-disable-next-line no-new
				new NavigationItem(document.name, document.url, item, '', null);
			}
		}
	}
	function buildGroups(reflections, parent, callback) {
		let state = -1;
		const hasExternals = containsExternals(reflections);
		sortReflections(reflections);
		reflections.forEach(function (reflection) {
			if (hasExternals && !reflection.flags.isExternal && state !== 1) {
				// eslint-disable-next-line no-new
				new NavigationItem('Internals', null, parent, 'tsd-is-external');
				state = 1;
			} else if (hasExternals && reflection.flags.isExternal && state !== 2) {
				// eslint-disable-next-line no-new
				new NavigationItem('Externals', null, parent, 'tsd-is-external');
				state = 2;
			}
			const item = NavigationItem.create(reflection, parent);
			includeDedicatedUrls(reflection, item);

			buildDocuments(item, true);

			if (callback) {
				callback(reflection, item);
			}

			buildDocuments(item, false);
		});
	}
	function build(hasSeparateGlobals) {
		const root = new NavigationItem('Index', 'index.html');
		if (entryPoint === project) {
			const globals = new NavigationItem('Index', hasSeparateGlobals ? 'globals.html' : 'index.html', root);
			globals.isGlobals = true;
		}
		const modules = [];
		project.getReflectionsByKind(ReflectionKind.SomeModule).forEach(function (someModule) {
			if (someModule.ignoreNav) return;

			let target = someModule.parent;
			let inScope = (someModule === entryPoint);
			while (target) {
				if (target.kindOf(ReflectionKind.ExternalModule)) {
					return;
				}
				if (entryPoint === target) {
					inScope = true;
				}
				target = target.parent;
			}
			if (inScope && someModule instanceof DeclarationReflection) {
				modules.push(someModule);
			}
		});

		buildGroups(entryPoint.getChildrenByKind(ReflectionKind.SomeModule), root, buildChildren);
		return root;
	}

	return build(this.application.options.getValue('readme') !== 'none');
};

module.exports = DefaultTheme;
