#!/bin/bash

# verify docker is installed
command -v docker >/dev/null 2>&1 || { echo >&2 "I require docker, but it is not installed.  Aborting."; exit 1; }

# change to our parent directory
cd ${0%/*}

# verify './image.cfg' exists
if [ ! -f "./image.cfg" ]; then
  echo "'./image.cfg' does not exist. Please create it"
  exit 1;
fi

# load variables from './image.cfg'
source "./image.cfg"

# check that required variables are defined
if [ -z ${image+x} ]; then echo "Variable 'image' not set in './image.cfg'"; exit 1; fi
if [ -z ${tag+x} ]; then echo "Variable 'tag' not set in './image.cfg'"; exit 1; fi

if [ "$tag" == "auto" ]; then
 if [ -z ${packagejson+x} ]; then echo "Variable 'tag' set to 'auto' and variable 'packagejson' is missing!"; exit 1; fi
 if [ ! -f "./${packagejson}" ]; then echo "Variable 'packagejson' points to non-existant file './${packagejson}'"; exit 1; fi

 autoParsed=$(cat "./${packagejson}" | grep -oP '(?<="version":\s")(\d+\.\d+.\d+)(?=".*)');
 if [ -z ${autoParsed+x} ]; then echo "Unable to determine version from './${packagejson}'"; exit 1; fi

 # add v to X.X.X
 tag="v${autoParsed}"
fi

build() {
  # check if $1 is set, this means we already have a tag defined
  if [ -z ${1+x} ]; then
   echo -n "Override image tag [${tag}]:"
   read tagOverride

   # update tag if need be
   if [ ! -z ${tagOverride} ]; then tag=${tagOverride}; fi
  else
   tag=${1};
  fi

  echo "Building ${image}:${tag}..."

  docker build -t "${image}:${tag}" .
}

push() {
  # check if $1 is set, this means we already have a tag defined
  if [ -z ${1+x} ]; then
   echo -n "Override image tag [${tag}]:"
   read tagOverride

   # update tag if need be
   if [ ! -z ${tagOverride} ]; then tag=${tagOverride}; fi
  else
   tag=${1};
  fi

  echo "Pushing ${image}:${tag}..."

  docker push "${image}:${tag}"
}

buildpush() {
  if [ -z ${1+x} ]; then
    echo -n "Override image tag [${tag}]:"
    read tagOverride

    if [ ! -z ${tagOverride} ]; then tag=${tagOverride}; fi
  else
    tag=${1}
  fi

  echo "Building and Pushing ${image}:${tag}..."

  build ${tag}
  push ${tag}
}

case $1 in
 build)
  if [ ! -z ${2} ]; then
    build $2
  else
    build
  fi
  ;;
 push)
   if [ ! -z ${2} ]; then
    push $2
  else
    push
  fi
  ;;
 buildpush)
  if [ ! -z ${2} ]; then
    buildpush $2
  else
    buildpush
  fi
  ;;
 *)
  echo "${0} [build|push|buildpush] (tag)"
  ;;
esac
